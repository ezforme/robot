package ru.robot.morpheyka;

import java.util.Scanner;

/**
 * <p>Updated by Morpheyka on 13.11.2016.</p>
 * @author Никита
 * @version 0.3
 *
 * Класс демо осуществляет демонстрацию данной программы.
 * <ul>Возможности</ul>
 * <li>Ввод начальных координат</li>
 * <li>Ввод координат финиша робота</li>
 * <li>Вывод на экран местонахождение робота в данный момент</li>
 */
public class Demo {
    public static void main(String[] args){
        Scanner inC = new Scanner(System.in);
        Point startPoint = new Point();
        Point finishPoint = new Point();

        startPoint.inputData(inC,"стартовую");
        finishPoint.inputData(inC,"конечную");

        Robot robot = new Robot(startPoint, Side.up);

        System.out.println("");
        System.out.print("В начале пути, данные о роботе - ");
        System.out.println(robot.toString());

        robot.walking(startPoint, finishPoint);

        System.out.println("");
        System.out.print("После пройденого пути, данные поменялись на - ");
        System.out.println(robot.toString());

    }
}
