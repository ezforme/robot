package ru.robot.morpheyka;

import java.util.Scanner;

/**
 * <p>Updated by Morpheyka on 13.11.2016.</p>
 * @author Никита
 * @version 0.3
 *
 * <p>Класс Point реализует точку на координатной плоскости</p>
 * Является родителем класса Robot
 *
 */
public class Point {
    private int x,y;

    /**
     * <p>Параметры двух точек координат.</p>
     *
      * @param x - абсцисса
     *  @param y - ординат
     */

    Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    Point(){
        x = 0;
        y = 0;
    }

    /**
     * Функция ввода данных.
     *
     * @param word - передает слово для уточнения вводимой координаты
     */

    public void inputData(Scanner in, String word){
        System.out.print("Введите " + word + " точку для робота по оси x: ");
        x = in.nextInt();
        System.out.print("Введите " + word + " точку для робота по оси у: ");
        y = in.nextInt();
    }

    int getX(){
        return x;
    }

    int getY(){
        return y;
    }

    void setX(int x){
        this.x = x;
    }

    void setY(int y){
        this.y = y;
    }


    @Override
    public String toString() {
        return "x = " + x + ", y = " + y + ".";
    }
}
