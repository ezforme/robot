package ru.robot.morpheyka;

/**
 * <p>Updated by Morpheyka on 13.11.2016.</p>
 * @author Никита
 * @version 0.3
 *
 * Класс Robot, наследник класса Point, позволяет производить манипуляции с роботом, точнее:
 * <ul>Он может</ul>
 * <li>Поворот направо</li>
 * <li>Поворот налево</li>
 * <li>Шаг</li>
 * <li>Ходьба</li>
 */

public class Robot extends Point {
    private Side side;

    /**
     * <p>Параметры робота</p>
     *
     * @param side - направление взгляда робота
     */

    Robot(Side side,int x,int y){
        super(x,y);
        this.side = side;
    }

    Robot(Point point, Side side){
        super(point.getX(),point.getY());
        this.side = side;
    }

    Robot(int x,int y){
        super(x,y);
        this.side = Side.up;
    }

    Robot(){
        super();
        this.side = Side.up;
    }

    /**
     *<p>Функция Шаг</p>
     * Осуществляет шаг в одну клетку по направлению робота
     */

    private void step(Point point){
        switch (side){
            case up:
                point.setY(getY() + 1);
                break;
            case right:
                point.setX(getX() + 1);
                break;
            case down:
                point.setY(getY() - 1);
                break;
            case left:
                point.setX(getX() - 1);
                break;
        }
    }


    /**
     * <p>Поворот вправо</p>
     * Поворачивает робота на 90 градусов по часовой стрелке
     */

    private void turnRight(){
        switch (side){
            case up:
                side = Side.right;
                break;
            case right:
                side = Side.down;
                break;
            case down:
                side = Side.left;
                break;
            case left:
                side = Side.up;
                break;
        }
    }

    /**
     * <p>Поворот влево</p>
     * Поворачивает робота на 90 градусов против часовой стрелке
     */

    private void turnLeft(){
        switch (side){
            case up:
                side = Side.left;
                break;
            case right:
                side = Side.up;
                break;
            case down:
                side = Side.right;
                break;
            case left:
                side = Side.down;
                break;
        }
    }

    /**
     * Функция поворачивает робота в нужное нам положение
     *
     * @param wantTurn - сторона в которую мы ходим, что бы робот смотрел
     */

    private void choiseCorrectTurn(Side wantTurn){
        while(side != wantTurn)
            turnRight();
    }


    /**
     * <p>Вспомогательная функция выполняет перемещения робота до конечной точки по прямой линии</p>
     *
     * @param start - объект содержащий в себе начальные точки робота
     * @param finish - объект содержащий в себе конечные точки робота
     * @param symbol - строка для определения по какой линии двигаться (х,у)
     */

    private void walk(Point start,Point finish, String symbol){
        if(symbol == "x"){
            while(start.getX() != finish.getX())
                step(start);
        } else {
            while (start.getY() != finish.getY())
                step(start);
        }
    }

    /**
     * <p>Вспомогательная функция осуществляет передвижение робота от начальной точки до финишной по одной координате,
     * с учетом поворота робота и разворотом, если это необходимо. </p>
     *
     * @param start  - объект содержащий в себе начальные точки робота
     * @param finish - объект содержащий в себе конечные точки робота
     * @param symbol - строка для определения по какой линии двигаться (х,у)
     */

    private void goToFinish(Point start, Point finish, String symbol) {

        if (symbol == "x") {
            if (start.getX() > finish.getX()) {
                choiseCorrectTurn(Side.left);
                walk(start, finish, "x");
            } else if (start.getX() < finish.getX()) {
                choiseCorrectTurn(Side.right);
                walk(start, finish, "x");
            }
        } else if (symbol == "y") {
            if(start.getY() > finish.getY()) {
                choiseCorrectTurn(side.down);
                walk(start, finish, "y");
            } else if(start.getY() < finish.getY()){
                choiseCorrectTurn(side.up);
                walk(start,finish,"y");
            }
        }
    }


    /**
     * <p>Функция совершает перемещение робота от начальной точки до конечной по обеим координатам, учитывая повороты</p>
     *
     * @param start - объект содержащий в себе начальные точки робота
     * @param finish - объект содержащий в себе конечные точки робота
     */
    public void walking(Point start, Point finish){
        goToFinish(start,finish,"x");
        goToFinish(start,finish,"y");
    }

    @Override
    public String toString() {
        return "взгляд робота = " + this.side + ", с точками " + super.toString();
    }
}
