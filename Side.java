package ru.robot.morpheyka;

/**
 * <p>Updated by Morpheyka on 09.11.2016.</p>
 * @author Никита
 * @version 0.2
 *
 * <p>Класс перечисления Side</p>
 * Содержит в себе 4 варианта направления взгляда робота
 */

public enum Side {
    /**
     * <ul>Стороны</ul>
     *
     * <li>left - влево</li>
     * <li>right - вправо</li>
     * <li>up - вверх</li>
     * <li>down - вниз</li>
     */

    left,right,up,down
}
